from application.models import User
from application import db


def add_user(user_name, email):
    new_user = User(username=user_name, email=email)
    db.session.add(new_user)
    db.session.commit()
    return new_user


def list_users():
    return User.query.all()
