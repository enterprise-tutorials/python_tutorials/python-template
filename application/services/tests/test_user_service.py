from application.services.user_service import list_users, add_user


def test_list_users():
    assert len(list_users()) > 0


def test_add_user():
    user_count = len(list_users())
    add_user("dummy", "dummy@dummy.com")
    assert len(list_users()) - user_count == 1
