import recurly
import logging
from application import app
from application import db
from application.models import RecurlyWebhook, UserSubscription, UserSubscriptionLog, Invoice

logger = logging.getLogger(__name__)

recurly.API_KEY = app.app_config['recurly'].get('api_key')
recurly.SUBDOMAIN = app.app_config['recurly'].get('subdomain')


# Monkey patching recurly

class Str(object):
    @classmethod
    def from_element(cls, elem):
        return (elem.text or '').strip()


recurly.Invoice._classes_for_nodename = {'subscription_id': Str}


def save_web_hook(data):
    """
    Saves the webhook data
    :param data:
    :return:
    """

    webhook = RecurlyWebhook(hookpayload=data, status=0)
    db.session.add(webhook)
    db.session.commit()
    # TODO: Move the below code to some queue - rq is recommended
    process_web_hook(webhook)
    webhook.status = 1
    db.session.add(webhook)
    db.session.commit()
    return webhook


def process_web_hook(webhook: RecurlyWebhook):
    """
    Processes a webhook
    :param webhook:
    :return:
    """

    payload = webhook.hookpayload
    notification = recurly.objects_for_push_notification(payload)
    notification_type = notification.get('type')

    logging.debug("Handling webhook %s", notification_type)

    if notification_type in (
            'renewed_subscription_notification',
            'new_subscription_notification',
            'expired_subscription_notification'
    ):
        recurly_subscription = notification.get('subscription')
        # Process subscription
        user_subscription = UserSubscription.query.filter(UserSubscription.uuid == recurly_subscription.uuid).first()
        if not user_subscription:
            # Create
            recurly_account = notification.get('account')
            user_subscription = UserSubscription(user_id=recurly_account.account_code,
                                                 username=recurly_account.username,
                                                 status='RENEW',
                                                 uuid=recurly_subscription.uuid,
                                                 plan=recurly_subscription.plan.plan_code)

        user_subscription_log = UserSubscriptionLog(status=notification_type)
        user_subscription.user_subscription_logs.append(user_subscription_log)
        db.session.add(user_subscription)
        db.session.commit()

    if notification_type in ('paid_charge_invoice_notification', 'new_charge_invoice_notification'):
        # Handle invoice
        recurly_invoice = notification.get('invoice')
        invoice = Invoice.query.filter(Invoice.uuid == recurly_invoice.uuid).first()
        if not invoice:
            invoice = Invoice(uuid=recurly_invoice.uuid,
                              total_in_cents=recurly_invoice.total_in_cents,
                              invoice_number=recurly_invoice.invoice_number,
                              status=recurly_invoice.state)

        # TODO: Check existence
        recurly_subscription_uuid = recurly_invoice.subscription_ids[0]
        user_subscription = UserSubscription.query.filter(
            UserSubscription.uuid == recurly_subscription_uuid).first()
        invoice.user_subscription_id = user_subscription.id
        invoice.status = recurly_invoice.state
        db.session.add(invoice)
        db.session.commit()


def handle_web_hook(data):
    """
    Handles the recurly webhook data
    :param data:
    :return:
    """
    pass
