import logging
from flask import render_template, jsonify, Response, request
from flask.views import View

from application.services.recurly_service import save_web_hook
from application.services.user_service import add_user, list_users

logger = logging.getLogger(__name__)


class ShowUsers(View):

    def dispatch_request(self):
        users = list_users()
        return jsonify([user.serialize() for user in users])


class AddUser(View):

    def dispatch_request(self):
        return jsonify(add_user("demo", "dummy@dummy.com").serialize())


class RecurlyWebHook(View):
    """
        Recurly webhook handler
    """

    methods = ['GET', 'POST']

    def dispatch_request(self):
        data = request.get_data(as_text=True).strip()
        try:
            webhook = save_web_hook(data)
        except Exception as e:
            logger.error("Error handling webhook", e)

        return Response("Done", status=200)
