"""create_recurly_webhook

Revision ID: ab6e719f6a9d
Revises: a730a2c765ec
Create Date: 2019-09-26 13:47:47.698726

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ab6e719f6a9d'
down_revision = '3c353b04dd98'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'recurly_webhook',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('hookpayload', sa.Text, nullable=True),
        sa.Column('status', sa.SmallInteger),
    )


def downgrade():
    op.drop_table('recurly_webhook')

