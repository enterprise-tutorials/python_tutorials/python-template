"""create_user_subscription

Revision ID: b70aada5b2dc
Revises: ab6e719f6a9d
Create Date: 2019-09-27 10:01:25.483662

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'b70aada5b2dc'
down_revision = 'ab6e719f6a9d'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'user_subscription',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id', sa.String(100), nullable=False),
        sa.Column('username', sa.String(100), nullable=False),
        sa.Column('uuid', sa.String(50), nullable=False),
        sa.Column('status', sa.String(30)),
        sa.Column('plan', sa.String(100), nullable=False)
    )

    op.create_table(
        'user_subscription_log',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_subscription_id', sa.Integer, nullable=False),
        sa.Column('status', sa.String(100)),
        sa.ForeignKeyConstraint(['user_subscription_id'], ['user_subscription.id'])
    )

    op.create_table(
        'invoice',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_subscription_id', sa.Integer, nullable=False),
        sa.Column('uuid', sa.String(50), nullable=False),
        sa.Column('invoice_number', sa.Integer, nullable=False),
        sa.Column('total_in_cents', sa.Integer, nullable=False),
        sa.Column('status', sa.String(30)),
        sa.ForeignKeyConstraint(['user_subscription_id'], ['user_subscription.id'])
    )


def downgrade():
    op.drop_table('user_subscription_log')
    op.drop_table('invoice')
    op.drop_table('user_subscription')


