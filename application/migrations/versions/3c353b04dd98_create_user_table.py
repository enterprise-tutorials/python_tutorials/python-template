"""create user table

Revision ID: 3c353b04dd98
Revises: 
Create Date: 2019-09-03 12:50:59.771843

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3c353b04dd98'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'user',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('username', sa.String(50), nullable=False),
        sa.Column('email', sa.Unicode(200)),
    )


def downgrade():
    op.drop_table('user')

