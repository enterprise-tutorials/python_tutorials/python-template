from application import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)

    def serialize(self):
        return {'id': self.id,
                'name': self.username,
                'email': self.email}


class RecurlyWebhook(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    hookpayload = db.Column(db.Text)
    status = db.Column(db.SmallInteger)


class UserSubscription(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.String(100))
    username = db.Column(db.String(100), nullable=False)
    uuid = db.Column(db.String(50), nullable=False)
    status = db.Column(db.String(30), nullable=False)
    plan = db.Column(db.String(100), nullable=False)
    user_subscription_logs = db.relationship("UserSubscriptionLog")


class UserSubscriptionLog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_subscription_id = db.Column(db.Integer, db.ForeignKey('user_subscription.id'))
    status = db.Column(db.String(100), nullable=False)


class Invoice(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_subscription_id = db.Column(db.Integer, db.ForeignKey('user_subscription.id'))
    uuid = db.Column(db.String(50), nullable=False)
    invoice_number = db.Column(db.Integer, nullable=False)
    total_in_cents = db.Column(db.Integer, nullable=False)
    status = db.Column(db.String(30), nullable=False)
