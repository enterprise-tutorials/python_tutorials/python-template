import os
import logging
from sqlalchemy import text
from flask import Flask, render_template, send_from_directory
from flask_sqlalchemy import SQLAlchemy

from application.config import init_config

app = Flask(
    __name__,
    instance_path=os.path.dirname(os.path.realpath(__file__)),
    static_url_path='/static')
app.url_map.strict_slashes = False

app.config.update(
    # Disable signals by default to get rid of warning about it going away in
    # the future.
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
)

init_config(app)
db = SQLAlchemy(app)

from application.views import ShowUsers, AddUser, RecurlyWebHook

app.add_url_rule('/users/', view_func=ShowUsers.as_view('show_users'))
app.add_url_rule('/users/add', view_func=AddUser.as_view('add_user'))
app.add_url_rule('/recurly/wh', view_func=RecurlyWebHook.as_view('recurly_wh'))

logger = logging.getLogger(__name__)

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)
