import logging
import logging.config
import os

import anyconfig

logger = logging.getLogger(__name__)


def config_context():
    _ENV_VAR_PREFIX = 'APP_CONFIG_'
    _ENV_VAR_PREFIX_LEN = len(_ENV_VAR_PREFIX)
    context = {}

    for (k, v) in list(os.environ.items()):
        if k.startswith(_ENV_VAR_PREFIX):
            context[k[_ENV_VAR_PREFIX_LEN:]] = v

    return context


def init_config(app):
    """
    Load configuration settings from a yaml file,
    Don't log or print these because they may have sensitive data.
    """
    config = get_config_file()

    context = config_context()

    app_config = anyconfig.load(
        [get_base_config_file(app), config],
        'yaml',
        ac_ignore_missing=False,
        ac_template=True,
        ac_context=context
    )

    # Add the flask section to the app.config.
    app.config.update(app_config['flask'])

    # All other sections are accessible in `app.app_config`
    app.app_config = app_config

    log_config = app_config.get('logging')

    if log_config:
        logging.config.dictConfig(app_config.get('logging'))
        logger.debug('Configured logging with:\n%s', log_config)
    else:
        logger.debug('Logging was not configured')


def get_base_config_file(app):
    """Get base yaml configuration file"""
    return os.path.join(app.root_path, 'config/base.yaml')


def get_config_file():
    config = os.environ.get('APP_CONFIG')
    if not config or not os.path.exists(config):
        raise RuntimeError(
            'APP_CONFIG environment variable must point to a yaml config file'
        )
    return config
