SHELL := /bin/bash
.SHELLFLAGS := -e -o pipefail -c
ifdef DEBUG_SHELL
.SHELLFLAGS += -x
endif
PROJECT_DIR := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))
BUILD_NUMBER ?= $(shell date +%s)
VERSION ?= 0.1
FULL_VERSION ?= $(VERSION).$(BUILD_NUMBER)
NUM_CORES := $(shell getconf _NPROCESSORS_ONLN)
OS := $(shell uname -s)
PIPENV := PIPENV_VENV_IN_PROJECT=1 TMPDIR=$(PROJECT_DIR)/tmp pipenv --bare

ifeq ($(OS), Darwin)
	# -i differs across GNU and BSD sed.
	SED := gsed
else
	SED := sed
endif

help:
	@echo "Development:"
	@echo "  rundev                  Run development server"
	@echo "  Pipfile.lock        	 Build frozen requirements.txt"
	@echo "  venv_dev                Creates pipenv venv"

rundev:
	APP_CONFIG=config.yml $(PIPENV) run python manage.py runserver

Pipfile.lock: Pipfile
	mkdir -p tmp
	if [[ -f Pipfile.lock ]]; then
		cp -f Pipfile.lock Pipfile.lock.old
	fi
	if ! ( $(PIPENV) lock --dev ); then
		echo 'Generating tmp/Pipefile.graph for dependency resolution...'
		if $(PIPENV) install --dev --skip-lock && $(PIPENV) graph > tmp/Pipfile.graph
			then echo "Failed to generate graph!"
		fi
		rm -rf .venv

		if [[ -f tmp/Pipfile.lock ]]
		then
			cp -f tmp/Pipfile.lock Pipfile.lock
			touch Pipfile
		else
			rm -f Pipfile.lock
		fi
		exit 1
	else
		rm -f tmp/Pipfile.lock
	fi

# Note: This intentionally does not create/update Pipfile.lock. If dependencies have changed
#  use the venv_dev target to perform a full update.
venv:
	$(PIPENV) sync

venv_dev: Pipfile.lock
	$(PIPENV) sync --dev

create_migration:
	APP_CONFIG=config.yml $(PIPENV) run python manage.py db revision -m $(RUNARGS)

.PHONY: clean all run help venv venv_dev rundev create_migration
.ONESHELL:
