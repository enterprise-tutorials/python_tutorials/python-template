#!/usr/bin/env python

from flask_script import Manager, Shell, Server
from flask_migrate import MigrateCommand, Migrate
from application import app, db, models
from application.config import get_base_config_file
from os import path

manager = Manager(app)


def _make_context():
    return dict(app=app, db=db, models=models)


manager.add_command(
    'runserver',
    Server(extra_files=[get_base_config_file(app)])
)

manager.add_command('db', MigrateCommand)
manager.add_command("shell", Shell(make_context=_make_context))

migrate = Migrate(app, db, directory=path.join(app.root_path, 'migrations'))

if __name__ == '__main__':
    manager.run()
