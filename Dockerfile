FROM python:3.6
ENV APP_ROOT=/app
RUN mkdir -p ${APP_ROOT}
WORKDIR ${APP_ROOT}
COPY . ${APP_ROOT}
COPY ./config.yml ${APP_ROOT}/config.yml
RUN pip install -U pipenv
RUN make venv
ENV APP_CONFIG=${APP_ROOT}/config.yml
COPY ./dockerrun.sh ${APP_ROOT}/run.sh
RUN chmod +x ${APP_ROOT}/run.sh
CMD "${APP_ROOT}/run.sh"