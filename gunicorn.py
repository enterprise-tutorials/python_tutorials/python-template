import multiprocessing

bind = '0.0.0.0:8080'
forwarded_allow_ips = '*'
workers = multiprocessing.cpu_count() * 2