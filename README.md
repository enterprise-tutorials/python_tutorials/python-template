# Template      

### Steps to run      
- Run the db - `docker-compose up` - Install pipenv - [https://github.com/pypa/pipenv#installation](https://github.com/pypa/pipenv#installation)
- Install dependencies (including dev) - `make venv_dev`
- Run migration - `pipenv run python manage.py db upgrade`
- Run dev server - `make rundev`

- Run test with coverage - `pipenv run coverage run -m pytest`
- View coverage console - `pipenv run coverage report`
- View coverage html - `pipenv run coverage html`
- Create db migration `make create_migration RUNARGS=<migrationname>`

#### Demo data testing Once the app is running - two urls will be available            
1. List users - [http://localhost:5000/users](http://localhost:5000/users)      
2. Adduser - [http://localhost:5000/users/add](http://localhost:5000/users/add)

### Steps to stop
- Open a new terminal and execute `docker-compose down`